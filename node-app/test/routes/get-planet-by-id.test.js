const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const sendInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();
const findByIdInvocation = sandbox.stub();

const {INTERNAL_SERVER_ERROR, NOT_FOUND} = require('http-status-codes');
const getPlanetById = proxyquire('../../routes/get-planet-by-id', {
    '../database/planet-data': {
        findById: findByIdInvocation
    }
});

describe('routes/get-planet-by-id', () => {
    describe('In case of success', () => {
        beforeEach(() => sandbox.reset());

        it('Should return the planet found and continue.', async () => {
            const response = {send: sendInvocation};
            const request = {params:{id: 'x'}};
            findByIdInvocation.resolves({id: '112358'});

            await getPlanetById(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce;
            expect(findByIdInvocation)
                .to.be.calledOnce
                .and.calledWith('x');
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o => o.id === '112358'));
        });
    });
    describe('In case of error', () => {
        beforeEach(() => sandbox.reset());
        it('Should forward an internal error in case of exception.', async () => {
            const response = {send: sendInvocation};
            const request = {params:{id: 'x'}};
            findByIdInvocation.rejects(new Error('thrown on purpose'));

            await getPlanetById(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === INTERNAL_SERVER_ERROR));
            expect(findByIdInvocation)
                .to.be.calledOnce
                .and.calledWith('x');
            expect(sendInvocation)
                .to.not.be.called;
        });
        it('Should forward a "not found" error if ID is not found', async () => {
            const response = {send: sendInvocation};
            const request = {params:{id: 'x'}};
            findByIdInvocation.resolves(null);

            await getPlanetById(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === NOT_FOUND));
            expect(findByIdInvocation)
                .to.be.calledOnce
                .and.calledWith('x');
            expect(sendInvocation)
                .to.not.be.called;
        });
    });
});