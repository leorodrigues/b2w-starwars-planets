module.exports = {
    'env': {
        'node': true,
        'mocha': true
    },
    'rules': {
        'max-len': ["error", 100]
    }
}