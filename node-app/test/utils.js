
class DummyError extends Error {
    constructor() {
        super('thrown on purpose');
    }
}

module.exports = {DummyError};