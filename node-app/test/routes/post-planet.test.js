const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const sendInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();
const savePlanetInvocation = sandbox.stub();
const countFilmAppearancesIvc = sandbox.stub();

const {INTERNAL_SERVER_ERROR} = require('http-status-codes');
const postPlanet = proxyquire('../../routes/post-planet', {
    '../database/planet-data': {
        save: savePlanetInvocation
    },
    '../services/swapi-service': {
        countFilmAppearances: countFilmAppearancesIvc
    }
});

describe('routes/post-planet', () => {
    describe('In case of success', async () => {
        beforeEach(() => sandbox.reset());

        it('Should save and reply the new entity.', async () => {
            const request = {body: {x: 'y'}};
            const response = {send: sendInvocation};
            savePlanetInvocation.resolves({id: '112358'});
            countFilmAppearancesIvc.resolves(1);

            await postPlanet(request, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
            expect(savePlanetInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    o => o.x === 'y' && o.filmAppearances === 1));
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o => o.id === '112358'));
        });
    });
    describe('In case of error', () => {
        beforeEach(() => sandbox.reset());

        it('Should forward an internal error upon database exception.', async () => {
            const request = {body: {x: 'y'}};
            const response = {send: sendInvocation};
            countFilmAppearancesIvc.resolves(1);
            savePlanetInvocation.rejects(new Error('thrown on purpose'));

            await postPlanet(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === INTERNAL_SERVER_ERROR));
            expect(savePlanetInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    o => o.x === 'y' && o.filmAppearances === 1));
            expect(sendInvocation).to.not.be.calledOnce;
        });
    });
});