# STAR WARS PLANETS API CHALLENGE

This project is the resolution of a challenge posed in a
job oportunity skill assessment.

## The challenge

Basically the challenge is to develop an API to provide a set of operations
on top of a Star Wars Planets database. Here are the details:

- It must employ the ReST API design principles
- For each planet, the following data must be manually provided and persisted:
    - Name
    - Wheather
    - Terrain
- For each planet, the number of apearences in Star Wars movies must be shown.
  This information must be obtained using the official public [Star Wars API][1].
- The following features must be implemented:
    - Add / Remove a planet
    - List all the planets
    - Search planet by name and/or ID

## The resolution

### Project structure
___

This project is developed using NodeJS and MongoDB. It uses 'docker'
and 'docker-compose' as a means to run the project locally.

The directory structure is divided in two:

- INSTALL_DIR/node-app: Houses the node application (as the name implies)
- INSTALL_DIR/volumes: Houses the MongoDB data files.

Using this structure, you can shutdown the database container without incurring
the risk of losing the persisted data.

### Requirements
___

**Option one: Have NodeJS 8.11 and MongoDB 3.6 installed**

With this configuration, you may just run the application directly from the
command line usig the details described in the sections bellow.

**Option two: Have docker and docker-compose installed**

Using docker-compose, you don't have to deal with all the hassle of meeting every
requirement for each component in the solution. Again, the details
of configuration and running are described in the sections bellow.

### Configuration
___

The two sets of environment variables bellow, are used by the Node Application:

- NODE_ENV: dev
- MONGO_SW_HOST: mongo
- SW_PLANETS_API_PORT: 9000
- SWAPI_PUBLIC_URL: https://swapi.co/

This first set is used to control internal system behavior and point to the
external Star Wars API. If ommitted, the values listed above will be assumed as
default.

The second set of variables bellow, should be used if you are behind a firewall.
You should use values simillar to the examples given. Note however that,
as these variables are used from inside the container, setting the proxy host
to '127.0.0.1' or 'localhost' will refer to the container itself and not to the
machine in which it is running on.

- HTTP_PROXY: http://192.168.0.1:3128
- HTTPS_PROXY: http://192.168.0.1:3129
- NO_PROXY: localhost,*.localdomain,*.some.domain.com
- STRICT_SSL: false

Under docker-compose, this project is using two files to declare the variables:
```.env.default``` containing the "generic" variables and ```.env.local``` holding
the proxy configuration. They are referenced inside ```docker-compose.yml```.

### Running
___

**Get the project**

Use the git client of your preference to clone the repository. Here is a command
line example:

```bash
$ git clone git@bitbucket.org:leorodrigues/b2w-starwars-planets.git
$
```

**Using node directly**

Make sure you have an instance of mongodb running somewhere in your network or
on your local machine and set the environment variable accordingly. After that,
change to the clonned directory and run the application using ```npm```:

```bash
$ cd b2w-starwars-planets/node-app
$ export MONGO_HOST_NAME=some_host_name
$ npm start
```

**Using docker-compose**

Change directory to where you have clonned the repository
then run docker-compose:

```bash
$ cd b2w-starwars-planets
$ sudo docker-compose up
```

Note: you may not need to use 'sudo'.

**Endpoints**

Once the project is running, the following endpoints will be available:

#### http://localhost:9000/planet

- GET
  - Description: List all currently saved planets. The list may be empty tho.
  - Request: The body of the request is ignored.
  - Response:
    - HTTP 200: Uppon success, the body contains list of saved planets.
    - HTTP 500: If an exception occurs.

- POST
  - Description: Creates a planet. It appends the amount of appearences
    on movies for the given planet prior to saving the document.
  - Request: It MUST obey the payload specification described bellow.
  - Response:
    - HTTP 200: Uppon success, the body contains the representation of
      the saved planet.
    - HTTP 400: If the request fails to adhere to the specification.
    - HTTP 500: If an exception occurs.

#### http://localhost:9001/planet/search

- POST
  - Description: Returns a list of planets matching the given criterion.
  - Request: The body of the request is specified bellow.
  - Response:
    - HTTP 200: Uppon success, the body contains a list of representations of
      reffered planets that meet the criterion. The list may be empty if there
      is no planet meeting the criterion.
    - HTTP 400: If the body fails to adhere to the specification bellow.
    - HTTP 500: If an exception occurs.


#### http://localhost:9001/planet/:id

- GET
  - Description: Returns a single planet identified by ':id', which MUST have
    a length of 24 hexdigits.
  - Request: The body of the request is ignored.
  - Response:
    - HTTP 200: Uppon success, the body contains representation of the
      reffered planet.
    - HTTP 400: If the id fails to satisfy the format specification above.
    - HTTP 404: If there is no planet saved under ':id'.
    - HTTP 500: If an exception occurs.

- DELETE
  - Description: Removes a planet from the database
  - Request: The body of the request is ignored.
  - Response:
    - HTTP 204: This operation returns no content. The result is always the same
      even if the given ':id' is not found.
    - HTTP 500: If an exception occurs.

#### Payload Specifications:

POST /planet:
```json
{
    // string, required
    "name": "...",

    // string, required
    "weather": "...",

    // string, required
    "terrain": "..."
}
```

POST /planet/search:
```json
{
    // string:regex, required
    "name": "..."
}
```

## Code design observations
___

**Routing modules**

Each routing module implements either a handler to a HTTP verb on an endpoint or
a single input validation. The handlers names all begin with either 'get',
'put', 'post' or 'delete' followed by some descriptive name. Route configuration
happens in the '/routes/index.js' file, performed by the 'mount' function. The
validation handlers are declared in the 'routes/support' module.

This approach reduces the amount of code in the main '/index.js' file which is
the entry point of the entire application and has the primary responsibility of
configuring the server instance.

The 'routes' directory also includes the jsonschemas used to validate the POST
requests.

**Data access**

Data access is isolated in 'database/planet-data'. This module implements the
queries and persistence logic in such a way that the rest of the app remains
ignorant about the technology used to persist data.

[1]: "https://swapi.co" "Star Wars API"