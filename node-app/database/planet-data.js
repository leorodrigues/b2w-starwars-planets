const {withPlanetsCollection, queryId, DatabaseError} = require('./support');

async function collectDocuments(cursor) {
    const result = [];
    while (await cursor.hasNext()) {
        result.push(await cursor.next());
    }
    return result;
}

async function remove(id) {
    try {
        return await withPlanetsCollection(async planets =>
            planets.deleteOne(queryId(id)));

    } catch(error) {
        throw new DatabaseError('Unable to remove from database', error);
    }
}

async function save(planet) {
    try {
        const status = await withPlanetsCollection(async planets =>
            planets.insertOne(planet));

        const result = {...planet};
        result._id = status.insertedId;
        return result;
    } catch(error) {
        throw new DatabaseError('Unable to save on database', error);
    }
}

async function findById(id) {
    try {
        return await withPlanetsCollection(async planets =>
            planets.findOne(queryId(id)));
    } catch(error) {
        throw new DatabaseError('Unable to query the database by id', error);
    }
}

async function findByName(name) {
    try {
        return await withPlanetsCollection(async planets => {
            return collectDocuments(planets.find({name: new RegExp(name)}));
        });
    } catch(error) {
        throw new DatabaseError('Unable to query the database by name', error);
    }
}

async function findAll() {
    try {
        return await withPlanetsCollection(async planets => {
            return collectDocuments(planets.find());
        });
    } catch(error) {
        throw new DatabaseError('Unable to query the database', error);
    }
}

module.exports = {remove, save, findAll, findById, findByName};