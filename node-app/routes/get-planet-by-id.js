const logService = require('../services/log-service');
const planetData = require('../database/planet-data');
const errors = require('restify-errors');

module.exports = async function(request, response, next) {
    try {
        const entity = await planetData.findById(request.params.id);
        if (entity) {
            response.send(entity);
            return next();
        }
        return next(new errors.NotFoundError());
    } catch(error) {
        logService.error(error.message);
        next(new errors.InternalError());
    }
};