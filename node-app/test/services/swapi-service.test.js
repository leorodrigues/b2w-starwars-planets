const proxyquire = require('proxyquire');
const sinon = require('sinon');
const chai = require('chai');
const {DummyError} = require('../utils');
const expect = chai.expect;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const inspector = sandbox.stub();
const nextInvocation = sandbox.stub();

const swapiService = proxyquire('../../services/swapi-service',
    {
        'restify-clients': {
            createJsonClient: function() {
                return {
                    get: function(uri, callback) {
                        const {error, request, response, obj} = inspector(uri);
                        callback(error, request, response, obj);
                    }
                };
            }
        }
    }
);

describe('services/swapi-service', () => {
    describe('#countFilmAppearances(planetName)', () => {
        beforeEach(() => { sandbox.reset(); });

        it('Should return the total number of films.', async () => {
            inspector.returns({obj: {count: 1, results: [{films: {length: 5}}]}});
            const result = await swapiService.countFilmAppearances('earth');
            expect(result).to.equal(5);
        });

        it('Should rethrow exceptions.', async () => {
            inspector.returns({error: new DummyError()});
            try {
                await swapiService.countFilmAppearances('earth');
            } catch(error) {
                expect(error).to.be.instanceOf(DummyError);
            }
        });
    });
});