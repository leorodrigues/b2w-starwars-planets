const proxyquire = require('proxyquire');
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const validateInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();

const {BadRequestError, InternalError} = require('restify-errors');
const support = proxyquire('../../routes/support',
    {'jsonschema': {Validator: function() { return {validate: validateInvocation}; }}}
);

const {preparePayloadValidation, validateIdFormat} = support;

const body = {};
const request = {body};
const response = {};
const payloadSchema = {};

describe('routes/support', () => {
    describe('#validateIdFormat(id)', () => {
        let idString;
        beforeEach(() => {
            sandbox.reset();
            idString = '1a2b3c4d5e6f7a8b9c0d1e2f';
        });
        it('Should succeed if "id" is a 24 hexdigit string.', () => {
            validateIdFormat({params: {id: idString}}, {}, nextInvocation);
            expect(nextInvocation).to.be.calledOnceWithExactly();
        });
        it('Should fail if "id" is less than 24 digits long.', () => {
            idString = idString.substring(0, 23);
            validateIdFormat({params: {id: idString}}, {}, nextInvocation);
            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    e => e instanceof BadRequestError));
        });
        it('Should fail if "id" is more than 24 digits long.', () => {
            idString = idString + 'b';
            validateIdFormat({params: {id: idString}}, {}, nextInvocation);
            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    e => e instanceof BadRequestError));
        });
        it('Should fail if "id" has at least one character not hexdigit.', () => {
            idString = idString.substring(0, 23) + 'g';
            validateIdFormat({params: {id: idString}}, {}, nextInvocation);
            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    e => e instanceof BadRequestError));
        });
    });

    describe('#preparePayloadValidation(payloadSchema)', () => {
        const validatePayload = preparePayloadValidation(payloadSchema);

        beforeEach(() => sandbox.reset());

        it('Should continue to next step if validation succeeds', () => {
            validateInvocation.returns({valid: true});

            validatePayload(request, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
        });

        it('Should fail with HTTP 400 if validation fails', () => {
            validateInvocation.returns({valid: false});

            validatePayload(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.to.be.calledWithMatch(error =>
                    error instanceof BadRequestError);
        });

        it('Should fail with HTTP 500 if exception occurs', () => {
            validateInvocation.throwsException(Error);

            validatePayload(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.to.be.calledWithMatch(error =>
                    error instanceof InternalError);
        });
    });
});