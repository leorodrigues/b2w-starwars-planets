
const getPlanetById = require('./get-planet-by-id');
const getAllPlanets = require('./get-all-planets');
const postPlanet = require('./post-planet');
const postPlanetSearch = require('./post-planet-search');
const deletePlanetById = require('./delete-planet-by-id');
const {validateIdFormat, preparePayloadValidation} = require('./support');

const postPlanetPayloadSchema = require(
    './schemas/post-planet-payload.json');

const postPlanetSearchPayloadSchema = require(
    './schemas/post-planet-search-payload.json');

function mount(server) {
    server.get(
        '/planet',
        getAllPlanets);

    server.get(
        '/planet/:id',
        validateIdFormat,
        getPlanetById);

    server.post(
        '/planet',
        preparePayloadValidation(postPlanetPayloadSchema),
        postPlanet);

    server.post(
        '/planet/search',
        preparePayloadValidation(postPlanetSearchPayloadSchema),
        postPlanetSearch);

    server.del(
        '/planet/:id',
        validateIdFormat,
        deletePlanetById);
}

module.exports = {mount};
