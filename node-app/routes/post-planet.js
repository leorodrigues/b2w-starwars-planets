const logService = require('../services/log-service');
const swapiClientService = require('../services/swapi-service');
const planetData = require('../database/planet-data');
const errors = require('restify-errors');

module.exports = async function(request, response, next) {
    try {
        const planet = request.body;
        planet.filmAppearances =
            await swapiClientService.countFilmAppearances(planet.name);
        const entity = await planetData.save(planet);
        response.send(entity);
        next();
    } catch(error) {
        logService.error(error.message);
        next(new errors.InternalError());
    }
};
