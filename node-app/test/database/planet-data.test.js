const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const deleteOneInvocation = sandbox.stub();
const findOneInvocation = sandbox.stub();
const findInvocation = sandbox.stub();
const insertOneInvocation = sandbox.stub();
const queryIdInvocation = sandbox.stub();
const exceptionTrigger = sandbox.stub();

const {DatabaseError} = require('../../database/support');
const planetData = proxyquire('../../database/planet-data', {
    './support': {
        queryId: queryIdInvocation,
        withPlanetsCollection: async function(callback) {
            exceptionTrigger();
            return callback({
                deleteOne: deleteOneInvocation,
                find: findInvocation,
                findOne: findOneInvocation,
                insertOne: insertOneInvocation
            });
        }
    }
});

describe('database/planet-data', () => {
    describe('#remove(id)', () => {
        beforeEach(() => sandbox.reset());

        it('Should try to remove one item from the collection by id.', async () => {
            queryIdInvocation.returns('y');

            await planetData.remove('x');

            expect(queryIdInvocation).to.be.calledOnceWith('x');
            expect(deleteOneInvocation).to.be.calledOnceWith('y');
        });

        it('Should rethrow underlying system errors.', async () => {
            exceptionTrigger.throwsException(new Error('thrown on purpose'));
            try {
                await planetData.remove('y');
            } catch(e) {
                expect(e).to.be.instanceOf(DatabaseError);
            }
        });
    });

    describe('#save(planet)', () => {
        beforeEach(() => sandbox.reset());

        it('Should return the object inserted with its id.', async () => {
            const planet = {name: 'x'};

            insertOneInvocation.resolves({insertedId: 1});
            const result = await planetData.save(planet);

            expect(insertOneInvocation).to.be.calledOnceWith(planet);
            expect(result).to.be.deep.equal({name: 'x', _id: 1});
        });

        it('Should rethrow underlying system errors.', async () => {
            exceptionTrigger.throwsException(new Error('thrown on purpose'));
            try {
                await planetData.save({name: 'y'});
            } catch(e) {
                expect(e).to.be.instanceOf(DatabaseError);
            }
        });
    });

    describe('#findAll()', () => {
        beforeEach(() => sandbox.reset());

        it('Should return all records from the underlying database system.', async () => {
            const availablePlanets = [{name: 'x'}, {name: 'y'}, {name: 'z'}];
            findInvocation.returns({
                hasNext: async () => availablePlanets.length > 0,
                next: async () => availablePlanets.pop()
            });

            const result = await planetData.findAll();

            expect(result).to.be.deep.equal([
                {name: 'z'}, {name: 'y'}, {name: 'x'}
            ]);
        });

        it('Should rethrow underlying system errors.', async () => {
            exceptionTrigger.throwsException(new Error('thrown on purpose'));
            try {
                await planetData.findAll();
            } catch(e) {
                expect(e).to.be.instanceOf(DatabaseError);
            }
        });
    });
    describe('#findById(id)', () => {
        beforeEach(() => sandbox.reset());

        it('Should return "null" if the specific id is missing from the db.', async () => {
            findOneInvocation.resolves(null);
            queryIdInvocation.returns('p');

            const result = await planetData.findById('z');

            expect(result).to.be.null;
            expect(queryIdInvocation).to.be.calledOnceWith('z');
            expect(findOneInvocation).to.be.calledOnceWith('p');
        });

        it('Should return a single object with the specific id.', async () => {
            findOneInvocation.resolves('t');
            queryIdInvocation.returns('m');

            const result = await planetData.findById('g');

            expect(result).to.be.equal('t');
        });

        it('Should rethrow underlying system errors.', async () => {
            exceptionTrigger.throwsException(new Error('thrown on purpose'));
            try {
                await planetData.findById('h');
            } catch(e) {
                expect(e).to.be.instanceOf(DatabaseError);
            }
        });
    });

    describe('#findByName(name)', () => {
        beforeEach(() => sandbox.reset());

        it('Should return empty list if criterion is not met by any document.', async () => {
            findInvocation.returns({hasNext: async () => false});

            const result = await planetData.findByName('z');

            expect(result).to.be.deep.equal([]);
            expect(findInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    q => q.name instanceof RegExp));
        });

        it('Should return documents meeting the criterion.', async () => {
            const availablePlanets = [{name: 'a'}, {name: 'b'}, {name: 'c'}];
            findInvocation.returns({
                hasNext: async () => availablePlanets.length > 0,
                next: async () => availablePlanets.shift()
            });

            const result = await planetData.findByName('k');

            expect(result)
                .to.be.deep.equal([{name: 'a'}, {name: 'b'}, {name: 'c'}]);

            expect(findInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(
                    q => q.name instanceof RegExp));
        });

        it('Should rethrow underlying system errors.', async () => {
            exceptionTrigger.throwsException(new Error('thrown on purpose'));
            try {
                await planetData.findByName('h');
            } catch(e) {
                expect(e).to.be.instanceOf(DatabaseError);
            }
        });
    });
});