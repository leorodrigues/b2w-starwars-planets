const winston = require('winston');
const {format} = winston;
const {combine, simple, colorize} = format;

module.exports = winston.createLogger({
    exitOnError: false,
    format: combine(colorize(), simple()),
    transports: [
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true
        })
    ]
});