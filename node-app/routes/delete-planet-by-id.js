const logService = require('../services/log-service');
const httpStatusCodes = require('http-status-codes');
const planetData = require('../database/planet-data');
const errors = require('restify-errors');

module.exports = async function(request, response, next) {
    try {
        await planetData.remove(request.params.id);
        response.status(httpStatusCodes.NO_CONTENT);
        response.end();
        next();
    } catch(error) {
        logService.error(error.message);
        next(new errors.InternalError());
    }
};
