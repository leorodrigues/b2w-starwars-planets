const logService = require('../services/log-service');
const planetData = require('../database/planet-data');
const errors = require('restify-errors');

module.exports = async function(request, response, next) {
    try {
        const nameSpec = request.body.name;
        const planets = await planetData.findByName(nameSpec);
        response.send({planets});
        next();
    } catch(error) {
        logService.error(error.message);
        next(new errors.InternalError());
    }
};
