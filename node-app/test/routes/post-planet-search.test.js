const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const sendInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();
const findByNameInvocation = sandbox.stub();

const {INTERNAL_SERVER_ERROR} = require('http-status-codes');
const postPlanetSearch = proxyquire('../../routes/post-planet-search', {
    '../database/planet-data': {
        findByName: findByNameInvocation
    }
});

describe('routes/post-planet-search', () => {
    describe('In case of success', () => {
        beforeEach(() => sandbox.reset());

        it('Should send an empty list if the database is empty.', async () => {
            const request = {body: {name: 'earth'}};
            const response = {send: sendInvocation};
            findByNameInvocation.resolves([]);

            await postPlanetSearch(request, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
            expect(findByNameInvocation)
                .to.be.calledOnceWith('earth');
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o =>
                    o.planets.length === 0));
        });

        it('Should send all planets meeting criterion in the database.', async () => {
            const request = {body: {name: 'earth'}};
            const response = {send: sendInvocation};
            findByNameInvocation.resolves([{id: '112358'}]);

            await postPlanetSearch(request, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
            expect(findByNameInvocation)
                .to.be.calledOnceWith('earth');
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o =>
                    o.planets[0].id === '112358'));
        });
    });
    describe('In case of error', () => {
        beforeEach(() => sandbox.reset());

        it('Should forward an internal error in case of exception', async () => {
            const request = {body: {name: 'earth'}};
            const response = {send: sendInvocation};
            findByNameInvocation.rejects(new Error('thrown on purpose'));

            await postPlanetSearch(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === INTERNAL_SERVER_ERROR));
            expect(findByNameInvocation)
                .to.be.calledOnceWith('earth');
            expect(sendInvocation).to.not.be.called;
        });
    });
});