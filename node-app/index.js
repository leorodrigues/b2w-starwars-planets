const MSG_BAD_REQUEST = 'Bad request';
const MSG_INTERNAL_ERROR = 'Internal server error';
const MSG_NOT_ALLOWED = 'Operation not allowed';
const MSG_NOT_FOUND = 'Resource not found';

const SERVER_PORT = parseInt(process.env.SW_PLANETS_API_PORT || 9000);

const logService = require('./services/log-service');
const restify = require('restify');
const routes = require('./routes');

const server = restify.createServer({
    name: 'star-wars-planets',
    version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

routes.mount(server);

server.on('restifyError', function(request, response, error, callback) {
    error.toJSON = function() {
        const code = error.statusCode || 500;
        let message = 'Indeterminate error';
        switch(code) {
            case 400: message = MSG_BAD_REQUEST; break;
            case 404: message = MSG_NOT_FOUND; break;
            case 405: message = MSG_NOT_ALLOWED; break;
            case 500: message = MSG_INTERNAL_ERROR; break;
        }
        return { message, code };
    };
    return callback();
});

server.on('SIGTERM', () => {
    logService.warn('Got SIGTERM');
    server.close((error) => {
        let exitCode = 0;
        if (error) {
            logService.error(error.message);
            exitCode = 1;
        }
        logService.info('Server offline.');
        process.exit(exitCode);
    });
});

server.listen(SERVER_PORT, function () {
    if (process.env.HTTP_PROXY) {
        logService.info(`Using http proxy: ${process.env.HTTP_PROXY}`);
    }
    if (process.env.HTTPS_PROXY) {
        logService.info(`Using https proxy: ${process.env.HTTPS_PROXY}`);
    }
    logService.info(`${server.name} ready and listening at ${server.url}`);
});
