const logService = require('../services/log-service');
const errors = require('restify-errors');
const {Validator} = require('jsonschema');

const validator = new Validator();

function preparePayloadValidation(payloadSchema) {
    return function validatePayload(request, response, next) {
        try {
            const outcome = validator.validate(request.body, payloadSchema);
            if (outcome.valid) {
                return next();
            }
            const msg = outcome.toString().replace('\n', ';');
            logService.warn(`Bad payload received: ${msg}`);
            next(new errors.BadRequestError());
        } catch(error) {
            logService.error(`During payload validation: ${error.message}`);
            next(new errors.InternalError());
        }
    };
}

function validateIdFormat(request, response, next) {
    if (/^[a-fA-F0-9]{24}$/.test(request.params.id)) {
        next();
    } else {
        next(new errors.BadRequestError());
    }
}

module.exports = {preparePayloadValidation, validateIdFormat};