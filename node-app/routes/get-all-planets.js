const logService = require('../services/log-service');
const planetData = require('../database/planet-data');
const errors = require('restify-errors');

module.exports = async function(request, response, next) {
    try {
        const planets = await planetData.findAll();
        response.send({planets});
        next();
    } catch(error) {
        logService.error(error.message);
        next(new errors.InternalError());
    }
};