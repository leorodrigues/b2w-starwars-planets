const {MongoClient, ObjectId} = require('mongodb');
const PORT = parseInt(process.env.MONGO_SW_PORT || 27017);
const HOST = process.env.MONGO_SW_HOST || 'localhost';
const DATABASE_NAME = process.env.MONGO_SW_DB_NAME || 'swplanets';

async function withPlanetsCollection(callback) {
    const url = `mongodb://${HOST}:${PORT}`;
    const connection = await MongoClient.connect(url, {useNewUrlParser: true});
    try {
        const db = await connection.db(DATABASE_NAME);
        const collection = await db.collection('planets');
        return callback(collection);
    } finally {
        await connection.close();
    }
}

function queryId(id) {
    return {_id: ObjectId(id)};
}

class DatabaseError extends Error {
    constructor(message, cause) {
        super(message + ' - ' + cause.message);
        this.cause = cause;
        this.stack = this.stack + '\n\n----- CAUSED BY -----\n\n' + cause.stack;
    }
}

module.exports = {withPlanetsCollection, queryId, DatabaseError};