const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const sendInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();
const findAllPlanetsInvocation = sandbox.stub();

const {INTERNAL_SERVER_ERROR} = require('http-status-codes');
const getAllPlanets = proxyquire('../../routes/get-all-planets', {
    '../database/planet-data': {
        findAll: findAllPlanetsInvocation
    }
});

describe('routes/get-all-planets', () => {
    describe('In case of success', () => {
        beforeEach(() => sandbox.reset());

        it('Should send an empty list if the database is empty.', async () => {
            const response = {send: sendInvocation};
            findAllPlanetsInvocation.resolves([]);

            await getAllPlanets({}, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
            expect(findAllPlanetsInvocation).to.be.calledOnce;
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o =>
                    o.planets.length === 0));
        });
        it('Should send all available planets in the database.', async () => {
            const response = {send: sendInvocation};
            findAllPlanetsInvocation.resolves([{id: '112358'}]);

            await getAllPlanets({}, response, nextInvocation);

            expect(nextInvocation).to.be.calledOnce;
            expect(findAllPlanetsInvocation).to.be.calledOnce;
            expect(sendInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(o =>
                    o.planets[0].id === '112358'));
        });
    });
    describe('In case of error', () => {
        beforeEach(() => sandbox.reset());

        it('Should forward an internal error in case of exception', async () => {
            const response = {send: sendInvocation};
            findAllPlanetsInvocation.rejects(new Error('thrown on purpose'));

            await getAllPlanets({}, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === INTERNAL_SERVER_ERROR));
            expect(findAllPlanetsInvocation).to.be.calledOnce;
            expect(sendInvocation).to.not.be.called;
        });
    });
});