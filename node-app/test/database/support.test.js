const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {DummyError} = require('../utils');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const connectInvocation = sandbox.stub();
const dbInvocation = sandbox.stub();
const collectionInvocation = sandbox.stub();
const closeInvocation = sandbox.stub();

const {ObjectId} = require('mongodb');
const {queryId, withPlanetsCollection} = proxyquire('../../database/support', {
    'mongodb': {
        ObjectId,
        MongoClient: { connect: connectInvocation }
    }
});

describe('database/support', () => {
    describe('#queryId(id)', () => {
        it('Should return a query using "_id" as an instance of "ObjectId".', () => {
            const idString = '000000000000000000000001';
            const result = queryId(idString);
            expect(result).to.be.deep.equal({_id: ObjectId(idString)});
        });
    });

    describe('#withPlanetsCollection(callback)', () => {
        beforeEach(() => sandbox.reset());

        it('Should close connection after callback completion.', async () => {
            connectInvocation.resolves(
                {db: dbInvocation, close: closeInvocation});
            dbInvocation.resolves({collection: collectionInvocation});
            collectionInvocation.resolves([1, 2, 3]);

            const result = await withPlanetsCollection(c => c);

            expect(result).to.be.deep.equal([1, 2, 3]);
            expect(closeInvocation).to.be.calledOnce;
        });

        it('Should close connection if the callback throws an error.', async () => {
            connectInvocation.resolves(
                {db: dbInvocation, close: closeInvocation});
            dbInvocation.resolves({collection: collectionInvocation});
            collectionInvocation.resolves([1, 2, 3]);

            try {
                await withPlanetsCollection(() => {
                    throw new DummyError();
                });
            } catch(error) {
                expect(error).to.be.instanceOf(DummyError);
                expect(closeInvocation).to.be.calledOnce;
            }
        });

        it('Should close connection if collection retrieval fails.', async () => {
            connectInvocation.resolves(
                {db: dbInvocation, close: closeInvocation});
            dbInvocation.resolves({collection: collectionInvocation});
            collectionInvocation.rejects(new DummyError());

            try {
                await withPlanetsCollection();
            } catch(error) {
                expect(error).to.be.instanceOf(DummyError);
                expect(closeInvocation).to.be.calledOnce;
            }
        });

        it('Should close connection if database retrieval fails.', async () => {
            connectInvocation.resolves(
                {db: dbInvocation, close: closeInvocation});
            dbInvocation.rejects(new DummyError());

            try {
                await withPlanetsCollection();
            } catch(error) {
                expect(error).to.be.instanceOf(DummyError);
                expect(closeInvocation).to.be.calledOnce;
            }
        });

        it('Should rethrow if the connection process itself fails.', async () => {
            connectInvocation.rejects(new DummyError());

            try {
                await withPlanetsCollection();
            } catch(error) {
                expect(error).to.be.instanceOf(DummyError);
                expect(closeInvocation).to.not.be.called;
            }
        });
    });
});