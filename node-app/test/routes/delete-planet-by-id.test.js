const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const {expect} = chai;

chai.use(require('sinon-chai'));

const sandbox = sinon.createSandbox();
const statusInvocation = sandbox.stub();
const endInvocation = sandbox.stub();
const nextInvocation = sandbox.stub();
const removePlanetInvocation = sandbox.stub();

const {INTERNAL_SERVER_ERROR, NO_CONTENT} = require('http-status-codes');
const deletePlanetById = proxyquire('../../routes/delete-planet-by-id', {
    '../database/planet-data': {
        remove: removePlanetInvocation
    }
});

describe('routes/delete-planet-by-id', () => {
    describe('In case of success', () => {
        beforeEach(() => sandbox.reset());

        it('Should signal success and remove a record.', async () => {
            const request = {params: {id: 'x'}};
            const response = {status: statusInvocation, end: endInvocation};

            await deletePlanetById(request, response, nextInvocation);

            expect(endInvocation)
                .to.be.calledOnce;
            expect(nextInvocation)
                .to.be.calledOnce;
            expect(removePlanetInvocation)
                .to.be.calledOnce
                .and.calledWith('x');
            expect(statusInvocation)
                .to.be.calledOnce
                .and.calledWith(NO_CONTENT);
        });
    });

    describe('In case of error', () => {
        beforeEach(() => sandbox.reset());

        it('Should forward internal error in case of exception.', async () => {
            const request = {params:{id: 'x'}};
            const response = {status: statusInvocation, end: endInvocation};
            removePlanetInvocation.rejects(new Error('thrown on purpose'));

            await deletePlanetById(request, response, nextInvocation);

            expect(nextInvocation)
                .to.be.calledOnce
                .and.calledWithMatch(sinon.match(e =>
                    e && e.statusCode === INTERNAL_SERVER_ERROR));
            expect(removePlanetInvocation)
                .to.be.calledOnce
                .and.calledWith('x');
            expect(statusInvocation)
                .to.not.be.called;
            expect(endInvocation)
                .to.not.be.called;
        });
    });
});