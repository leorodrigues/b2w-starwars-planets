const restify = require('restify-clients');

const clientOptions = {
    url: process.env.SWAPI_PUBLIC_URL || 'https://swapi.co/'
};

const client = restify.createJsonClient(clientOptions);

function get(uri) {
    return new Promise((resolve, reject) => {
        client.get(uri, (error, request, response, obj) => {
            if (error)
                return reject(error);
            return resolve(obj);
        });
    });
}

async function countFilmAppearances(planetName) {
    const query = await get(`/api/planets/?search=${planetName}`);
    if (query.count === 0)
        return 0;
    return query.results[0].films.length;
}

module.exports = {countFilmAppearances};